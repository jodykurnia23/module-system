class Table {
  constructor(init) {
   this.init = init;
 }

  createHeader(data) {
   let open = "<thead><tr>";
   let close = "</tr></thead>";
   data.forEach((d) => {
     open += `<th>${d}</th>`;
   });

   return open + close;
 }

  createBody(data) {
   let open = "<tbody>";
   let close = "</tbody>";

   data.forEach((d) => {
     open += `
       <tr>
         <td>${d[0]}</td>
         <td>${d[1]}</td>
         <td>${d[2]}</td>
         <td>${d[3]}</td>
       </tr>
     `;
   });

   return open + close;
 }

  render(element) {
   let table =
     "<table class='table table-hover border border-3'>" +
     this.createHeader(this.init.columns) +
     this.createBody(this.init.data) +
     "</table>";
   element.innerHTML = table;
 }
}

const table = new Table({
  columns: ["Name", "Email", "Alamat", "Jenis Kelamin"],
  data: [
    ["Rizka Maulia", "rizkamaulia5@gmail.com", "Griya Husada Cibitung", "Wanita"],
    ["Boyke Wiliyam", "boywiliyam11@gmail.com", "Dukuh Zambrud Bekasi", "Pria"],
    ["Nabila Siti Nurjannah", "nabilasiti2@gmail.com", "Bekasi Timur Regency", "Wanita"],
    ["Rizky Ramadhan", "ikyaja01@gmail.com", "Perum Vida Bekasi", "Pria"],
    ["Caca", "cacaenakloh4@gmail.com", "Perum Grand Residence", "Wanita"]
  ]
});
const app = document.getElementById("app");
table.render(app);

  